# README #

Kerangka kerja autentikasi mengunakan HySS, SQLite, dan algoritma hash untuk sistem login dan registrasi pengguna (user management system).

Hak Cipta (C) 2020 Achmad Koko <achmadkoko9921@gmail.com>

Lisensi MIT, lihat file LICENSE.txt

Paket kode sumber dapat ditemukan di [BitBucket](https://bitbucket.org/achmadkoko/hyss-sqlite-auth/) dan [OSDN](https://osdn.net/projects/hyss-sqlite-auth).

Secara umum kode sumber pada paket ini menggunakan HySS dan SQLite di bagian backend.

**PERINGATAN KEAMANAN: PASTIKAN UNTUK MENGONTROL DAN MENGUBAH PENGATURAN KEAMANAN PADA FILE user.hyss**

Skrip autentikasi yang ada di file **user.hyss** disajikan dengan mudah dan kamu dapat memodifikasinya. Tempatkan file **user.hyss** sehingga dapat diakses oleh skrip-skrip lain, dan kemudian awali setiap skrip HySS lain yang menggunakannya dengan baris:

	require_once("user.hyss");
	$USER = new User();

Berdasarkan hal ini maka akan terdapat variabel $USER tersedia di dalam skrip, dengan: 

	$USER->authenticated: boolean, yang menyajikan status login pengguna.
	$USER->username: username, atau "guest user" jika tidak terautentikasi.
	$USER->email: email pengguna, atau string kosong untuk guest users.
	$USER->role: peran. Secara default semua pengguna baru adalah "user".
	$USER->userdir: direktori data pengguna, false jika tidak terautentikasi.

Ada cara kedua untuk membuat pengguna baru, yang dapat melewatkan cara pertama melalui pemanggilan nama fungsi ketika pengguna baru akan daftar. Cara kedua ini khususnya digunakan dalam skrip sebagai titik masuk utama: 

	require_once("user.hyss");
	$USER = new User("registration_completed_function_name");

yang akan memanggil fungsi sebagai:

	registration_completed_function_name($username, $email, $userdir)

Ini memberi fungsi tadi nama pengguna yang baru terdaftar, alamat email, dan lokasi direktori data. Hal ini memungkinkan untuk mengikuti proses pendaftaran untuk memastikan bahwa tugas apa pun yang diperlukan saat pengguna baru bergabung (seperti mengolah file atau database untuk sistem kamu sendiri).

Sebagai catatan, tidak masalah apakah kamu menggunakan satu skrip entri, atau beberapa skrip berbeda. Jika kamu memiliki enam skrip untuk melakukan berbagai tugas dengan file-file terpisah (artinya tidak memerlukan atau menyertakan satu sama lain), cukup tambahkan baris **require_once(...)** dan **new User()** di awal setiap file, dan setiap skrip akan dapat menangani pengguna yang diautentikasi dengan session. Cukup buat tugas-tugas kondisional pada **$USER->authenticated**. Ini termasuk skrip yang kamu panggil melalui permintaan **GET** dan **POST** melalui **XHR** (permintaan "ajax").

